;(function() {
    "use strict";

    //                  0    1    2    3    4    5   6   7    8    9  10
    var volumeValues = [0, .01, .03, .07, .12, .2, .25, .38, .55, .7, 1];
    var player = {};
    player.audio = new Audio();
    player.status = 'stop'; //stop, play, pause
    player.volume = 5;
    player.list = [];
    player.root = '';
    player.currentId = 0;
    player.shuffle = 0;
    player.trueSort = [];
    player.repeat = 0;
    player.shuffleId = 0;

    chrome.browserAction.setBadgeBackgroundColor({color: [80,80,80,245]});

    var framesCounter = Math.floor(new Date());

    if(localStorage['shuffle'] && localStorage['trueSort'] && localStorage['trueSort'] != 'undefined') {

        player.shuffle = 1;
        player.trueSort = JSON.parse(localStorage['trueSort']);
    }

    if(localStorage['root']) {

        player.root = localStorage['root'];
    }

    if(localStorage['volume']) {

        player.volume = Math.floor(localStorage['volume']);
        player.audio.volume = volumeValues[player.volume];
    }

    if(localStorage['list']) {

        player.list = JSON.parse(localStorage['list']);
    }

    if(localStorage['id']) {

        player.currentId = Math.floor(localStorage['id']);
        player.shuffleId = getAfterShuffleId(player.currentId);
    }

    if(localStorage['currentTime']) {

        player.audio.currentTime = Math.floor(localStorage['currentTime']);
    }

    if(localStorage['repeat']) {

        player.repeat = Math.floor(localStorage['repeat']);
    }

    function playId(id) {

        player.shuffleId = getAfterShuffleId(id);

        if(!((typeof player.list == 'array' || typeof player.list == 'object') && typeof player.list[player.shuffleId] == 'string')) {

            return;
        }

        var root = player.root;

        // если указан путь от корня - его и ставим
        if(player.list[player.shuffleId].substr(0, 1) == '/') {

            root = root.replace(/^(.+\/\/.+?)\/.+$/, "$1");
        }

        localStorage['id'] = player.currentId = id;

        player.audio.src = root + player.list[player.shuffleId];

        if(player.status == 'play') {

            player.audio.play();
        }
    }

    chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {

        if(typeof request.type != 'undefined') {

            if(request.type == 'play') {

                if(player.status == 'pause') {

                    player.audio.play();
                    player.status = 'play';
                } else if(player.status == 'play') {

                    player.audio.pause();
                    player.status = 'pause';
                } else if(player.status == 'stop') {

                    player.status = 'play';
                    playId(player.currentId);
                }

                sendResponse(player.status);
            }
            if(request.type == 'prev') {

                if(player.currentId > 0) {

                    player.currentId--;
                    playId(player.currentId);

                    sendResponse(player.currentId);
                }
            }
            if(request.type == 'next') {

                if(player.currentId < (player.list.length - 1)) {

                    player.currentId++;
                    playId(player.currentId);

                    sendResponse(player.currentId);
                }
            }
            if(request.type == 'volume-up') {

                if(parseFloat(player.volume) < 10) {

                    player.volume = Math.floor(player.volume) + 1;
                }

                player.audio.volume = volumeValues[player.volume];
                localStorage['volume'] = player.volume;
                sendResponse(player.volume);
            }
            if(request.type =='volume-down') {

                if(parseFloat(player.volume) > 0) {

                    player.volume = Math.floor(player.volume) - 1;
                }

                player.audio.volume = volumeValues[player.volume];
                localStorage['volume'] = player.volume;
                sendResponse(player.volume);
            }
            if(request.type == 'get-volume') {

                sendResponse(player.volume);
            }
            if(request.type == 'load-files-list') {

                player.list = JSON.parse(localStorage['list']);
                sendResponse(player.list);
            }
            if(request.type == 'set-files-list') {

                var list = JSON.stringify(request.list);

                if(list) {

                    localStorage['shuffle'] = 0;

                    player.list = request.list;
                    player.shuffle = 0;
                    localStorage['list'] = list;
                    player.audio.url = player.root + player.list[0];
                    localStorage['id'] = player.currentId = 0;
                }

                sendResponse(list);
            }
            if(request.type == 'set-current-root') {

                localStorage['root'] = request.root;
                player.root = request.root;

                sendResponse(request.root);
            }
            if(request.type == 'set-current-file') {

                player.currentId = request.id;

                sendResponse(true);
            }
            if(request.type == 'get-current-counter') {

                var shuffle = getAfterShuffleId(player.currentId);
                sendResponse({'current': player.currentId, 'shuffle': shuffle, 'all': player.list.length});
            }
            if(request.type == 'get-current-file') {

                sendResponse(typeof player.list[player.shuffleId] == 'undefined' ? '&nbsp;' : player.list[player.shuffleId]);
            }
            if(request.type == 'get-status') {

                sendResponse(player.status);
            }
            if(request.type == 'get-shuffle') {

                sendResponse(player.shuffle);
            }
            if(request.type == 'get-repeat') {

                sendResponse(player.repeat);
            }
            if(request.type == 'change-shuffle') {

                player.shuffle = 1 - player.shuffle;
                localStorage['shuffle'] = player.shuffle;

                if(player.shuffle) {

                    var array = createArrayFromTo(0, player.list.length);

                    player.trueSort = shuffleArray(array);
                    localStorage['trueSort'] = JSON.stringify(player.trueSort);
                }

                sendResponse(player.shuffle);
                playId(0);
            }
            if(request.type == 'change-repeat') {

                player.repeat = 1 - player.repeat;
                localStorage['repeat'] = player.repeat;

                sendResponse(player.repeat);
            }
        };
    });

    function createArrayFromTo(min, max) {

        var i = min, array = [];

        do {

            array.push(i);
            i++;
        } while(i < max)

        return array;
    }

    function shuffleArray(array) {

        var currentIndex = array.length, temporaryValue, randomIndex;

        while (0 !== currentIndex) {

            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex--;

            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }

    function getAfterShuffleId(id) {

        if(player.shuffle) {

            id = player.trueSort[id];
        }

        return id;
    }

    function step(time) {

        if(framesCounter < Math.floor(time)) {

            framesCounter = Math.floor(time);

            if(player.status == 'pause') {

                chrome.browserAction.setBadgeText({text: 'pause'});
            } else if (player.status == 'play') {

                var _t = getTime(Math.floor(player.audio.duration - player.audio.currentTime));
                chrome.browserAction.setBadgeText({text: '-' + _t.m + ':' + _t.s});
            }

            var currentTime = Math.floor(player.audio.currentTime);
            if(currentTime % 5 == 0 && localStorage['currentTime'] != currentTime) {

                localStorage['currentTime'] = currentTime;
            }

            chrome.runtime.sendMessage({
                'type': 'time',
                'time': {
                    'current': player.audio.currentTime,
                    'duration': player.audio.duration
                }
            });

            if((player.audio.currentTime + .301) > player.audio.duration && (player.currentId < (player.list.length - 1) || player.repeat)) {

                setTimeout(function() {

                    player.currentId += (1 - player.repeat);
                    playId(player.currentId);
                }, 290);
            }
        }

        setTimeout(function() {
            step(Math.floor(new Date()));
        }, 300);
    };

    function getTime(_t) {

        var time = {};

        time.m = Math.floor(_t / 60);
        time.s = Math.floor(_t - time.m * 60);
        time.s = time.s1 < 10 ? ('0' + time.s) : time.s;

        return time;
    }

    step(framesCounter);
})();
