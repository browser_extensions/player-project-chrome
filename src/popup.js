;(function() {
    "use strict";

    chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {

        if (request.type == 'time') {

            setTime(request.time);
        }
    });

    function sendData(data, response) {

        chrome.runtime.sendMessage(data, function sendMessage(_r) {

            response(_r);
        });
    }

    function currentName() {

        sendData({'type': 'get-current-file'}, function _sendData(data) {

            var file = decodeURIComponent(data);

            document.querySelector('#name span').innerHTML = file;
        });
    }

    function currentCounter() {

        sendData({'type': 'get-current-counter'}, function _sendData(data) {

            var current = data.shuffle == data.current ? (1 + Math.floor(data.current)) : ((1 + Math.floor(data.current)) + '(' + (1 + Math.floor(data.shuffle)) + ')');
            document.querySelector('#current-counter span').innerHTML = current + '/' + data.all;
        });
    }

    function getStatus() {

        sendData({'type': 'get-status'}, function _sendData(data) {

            document.querySelector('#status span').innerHTML = data;
        });
    }

    function getVolume() {

        sendData({'type': 'get-volume'}, function _sendData(data) {

            document.querySelector('#volume-level span').innerHTML = (data * 10 + '%');
        });
    }

    function getShuffleStatus() {

        sendData({'type': 'get-shuffle'}, function _sendData(data) {

            var checkBox = document.querySelector('#shuffle');
            if(data) {

                checkBox.setAttribute('checked', 1);
            } else {

                checkBox.removeAttribute('checked');
            }
            currentName();
        });
    }

    function getRepeatStatus() {

        sendData({'type': 'get-repeat'}, function _sendData(data) {

            var checkBox = document.querySelector('#repeat');

            if(data) {

                checkBox.setAttribute('checked', 1);
            } else {

                checkBox.removeAttribute('checked');
            }
        });
    }

    function setTime(_t) {

        if(typeof _t != 'object') {

            return;
        }

        var time = {};

        time.m1 = Math.floor(_t.current / 60);
        time.m2 = Math.floor(_t.duration / 60);

        time.s1 = Math.floor(_t.current - time.m1 * 60);
        time.s2 = Math.floor(_t.duration - time.m2 * 60);

        time.s1 = time.s1 < 10 ? ('0' + time.s1) : time.s1;
        time.s2 = time.s2 < 10 ? ('0' + time.s2) : time.s2;

        document.querySelector('#time span').innerHTML = time.m1 + ':' + time.s1 + ' / ' + time.m2 + ':' + time.s2;
    }

    document.getElementById('load-links').addEventListener('click', function loadLinks() {

        chrome.tabs.query({active: true, currentWindow: true}, function tabsQuery(tabs) {

            chrome.tabs.sendMessage(tabs[0].id, {'type': 'getAllLinks'}, {}, function sendMessage(list) {

                sendData({'type': 'set-files-list', 'list': list}, function _sendData(data) {

                    if(data) {

                        chrome.tabs.query({'active': true, 'windowId': chrome.windows.WINDOW_ID_CURRENT},
                            function tabsQuery(tabs) {

                                var url = tabs[0].url;

                                if(url.substr(-1) !== "/") {

                                    url = url + "/";
                                }

                                sendData({'type': 'set-current-root', 'root': url}, function _sendData(_r) {

                                    if(_r) {

                                        document.querySelector('#name span').innerHTML = 'OK';

                                        setTimeout(function timeout() {

                                            currentName();
                                        }, 1000);
                                    } else {

                                        alert('Не могу установить корневую директорию для файлов');
                                    }
                                });
                            }
                        );
                    } else {

                        alert('нет файлов для загрузки?');
                    }
                });
            });
        });
    });

    document.getElementById('shuffle').addEventListener('click', function addEventListener() {

        sendData({'type': 'change-shuffle'}, function _sendData(data){

            getShuffleStatus();
            currentCounter();
        });
    });

    document.getElementById('repeat').addEventListener('click', function addEventListener() {

        sendData({'type': 'change-repeat'}, function _sendData(data){

            getRepeatStatus();
        });
    });

    document.getElementById('volume-up').addEventListener('click', function addEventListener() {

        sendData({'type': 'volume-up'}, function _sendData(data){

            getVolume();
        });
    });

    document.getElementById('volume-down').addEventListener('click', function addEventListener() {

        sendData({'type': 'volume-down'}, function _sendData(data){

            getVolume()
        });
    });

    document.getElementById('next').addEventListener('click', function addEventListener() {

        sendData({'type': 'next'}, function _sendData(){

            currentName();
            getStatus();
            currentCounter();
        });
    });

    document.getElementById('prev').addEventListener('click', function addEventListener() {

        sendData({'type': 'prev'}, function _sendData(){

            currentName();
            getStatus();
            currentCounter();
        });
    });

    document.getElementById('play-pause').addEventListener('click', function addEventListener() {

        sendData({'type': 'play'}, function _sendData(status){

            getStatus();
        });
    });

    currentName();
    getStatus();
    getVolume();
    getShuffleStatus();
    getRepeatStatus();
    setTime();
    currentCounter();
})();
