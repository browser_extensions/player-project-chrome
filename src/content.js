;(function() {
    "use strict";

    chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {

        if(typeof request.type == 'undefined') {

            return;
        }

        if (request.type == 'getAllLinks') {

            var list = [];
            var _list = document.querySelectorAll('a[href$=".mp3"]')

            for(var i = 0; i < _list.length; i++) {

                list.push(_list[i].getAttribute('href'));
            }

            sendResponse(list);
        }
    });
})();